package com.example.projet_ac.services;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import com.example.projet_ac.entities.Player;
import com.example.projet_ac.entities.Game;

public class LocalDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "local_db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_PLAYERS = "players";
    private static final String COLUMN_PLAYER_ID = "id";
    private static final String COLUMN_PLAYER_USERNAME = "username";
    private static final String COLUMN_PLAYER_COUNTRY = "country";
    private static final String COLUMN_PLAYER_RATING = "rating";

    private static final String TABLE_GAMES = "games";
    private static final String COLUMN_GAME_ID = "id";
    private static final String COLUMN_GAME_DATE = "date";
    private static final String COLUMN_GAME_LOSER = "loser";
    private static final String COLUMN_GAME_WINNER = "winner";
    private static final String COLUMN_GAME_GEOLOC = "geoloc";
    private static final String COLUMN_GAME_IMAGE = "image";

    public LocalDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_PLAYERS = "CREATE TABLE " + TABLE_PLAYERS +
                "(" + COLUMN_PLAYER_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_PLAYER_USERNAME + " TEXT, " +
                COLUMN_PLAYER_COUNTRY + " TEXT, " +
                COLUMN_PLAYER_RATING + " INTEGER)";
        db.execSQL(CREATE_TABLE_PLAYERS);

        String CREATE_GAMES_TABLE = "CREATE TABLE " + TABLE_GAMES + "(" +
                COLUMN_GAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_GAME_DATE + " TEXT NOT NULL," +
                COLUMN_GAME_LOSER + " TEXT NOT NULL," +
                COLUMN_GAME_WINNER + " TEXT NOT NULL, " +
                COLUMN_GAME_GEOLOC + " TEXT NOT NULL, " +
                COLUMN_GAME_IMAGE + " TEXT NOT NULL " + ")";
        db.execSQL(CREATE_GAMES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GAMES);
        onCreate(db);
    }
    public void addPlayer(String username, String country, int rating) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PLAYER_USERNAME, username);
        values.put(COLUMN_PLAYER_COUNTRY, country);
        values.put(COLUMN_PLAYER_RATING, rating);
        db.insert(TABLE_PLAYERS, null, values);
        db.close();
    }

    public List<Game> getGamesByPlayer(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sqlQuery = "SELECT * FROM " + TABLE_GAMES +
                " WHERE " + COLUMN_GAME_WINNER + "=?" + " OR " + COLUMN_GAME_LOSER + "=?";
        String[] selectionArgs = {username, username};
        Cursor cursor = db.rawQuery(sqlQuery, selectionArgs);
        List<Game> games = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                @SuppressLint("Range") int id = cursor.getInt(cursor.getColumnIndex(COLUMN_GAME_ID));
                @SuppressLint("Range") String date = cursor.getString(cursor.getColumnIndex(COLUMN_GAME_DATE));
                @SuppressLint("Range") String loser_ = cursor.getString(cursor.getColumnIndex(COLUMN_GAME_LOSER));
                @SuppressLint("Range") String winner_ = cursor.getString(cursor.getColumnIndex(COLUMN_GAME_WINNER));
                @SuppressLint("Range") String geoloc = cursor.getString(cursor.getColumnIndex(COLUMN_GAME_GEOLOC));
                @SuppressLint("Range") String image = cursor.getString(cursor.getColumnIndex(COLUMN_GAME_IMAGE));
                Game game = new Game(id, date, loser_, winner_, geoloc, image);
                games.add(game);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return games;
    }

    public void updatePlayerRating(String username, int newRating) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PLAYER_RATING, newRating);
        db.update(TABLE_PLAYERS, values, COLUMN_PLAYER_USERNAME + " = ?", new String[]{username});
        db.close();
    }

    public void addGame(String date, String loser, String winner) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_GAME_DATE, date);
        values.put(COLUMN_GAME_LOSER, loser);
        values.put(COLUMN_GAME_WINNER, winner);
        db.insert(TABLE_GAMES, null, values);
        db.close();
    }

}
