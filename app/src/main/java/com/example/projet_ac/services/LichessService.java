package com.example.projet_ac.services;

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import com.example.projet_ac.entities.Player;
import com.google.gson.*;

public class LichessService {

    public static void main(String[] args) {
        System.out.println(getTop10Players().toString());
    }

    public static Player getPlayer(String username){
        try {
            URL urlUser = new URL("https://lichess.org/api/user/" + username);

            HttpURLConnection connectionUser = (HttpURLConnection) urlUser.openConnection();

            // Set request method
            connectionUser.setRequestMethod("GET");

            // Read the response
            BufferedReader readerUser = new BufferedReader(new InputStreamReader(connectionUser.getInputStream()));
            StringBuilder responseUser = new StringBuilder();
            String lineUser;
            while ((lineUser = readerUser.readLine()) != null) {
                responseUser.append(lineUser);
            }
            readerUser.close();
            Gson gson = new Gson();

            String jsonResponseUser = responseUser.toString();
            JsonObject jsonObjectUser = gson.fromJson(jsonResponseUser, JsonObject.class);
            int rating = jsonObjectUser.getAsJsonObject("perfs")
                    .getAsJsonObject("classical")
                    .get("rating").getAsInt();
            JsonObject profileObjectUser = jsonObjectUser.getAsJsonObject("profile");
            String country;
            try{
                country = profileObjectUser.get("location").getAsString();
            }
            catch (Exception e){
                country = null;
            }
            return new Player(username, country, rating);
        }catch(Exception e){
            e.printStackTrace();
        }
        return new Player(null, null, 0);
    }
    public static List<Player> getTop10Players() {
        List<Player> listPlayer = new ArrayList<>();
        try {
            // Create URL object with the API endpoint
            URL url = new URL("https://lichess.org/api/player/top/10/classical");

            // Open a connection to the URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Set request method
            connection.setRequestMethod("GET");

            // Read the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            String jsonResponse = response.toString();
            // Close the connection
            connection.disconnect();
            JsonObject jsonObject = new Gson().fromJson(jsonResponse, JsonObject.class);
            JsonArray usersArray = jsonObject.getAsJsonArray("users");

            for (JsonElement userElement : usersArray) {
                JsonObject userObject = userElement.getAsJsonObject();
                String username = userObject.get("username").getAsString();
                listPlayer.add(getPlayer(username));
            }
            return listPlayer;
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
