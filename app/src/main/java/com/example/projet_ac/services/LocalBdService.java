package com.example.projet_ac.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.projet_ac.entities.Player;
import com.example.projet_ac.entities.Game;

public class LocalBdService {
    private static final String DATABASE_URL = "jdbc:sqlite:C:\\Users\\kekel\\OneDrive\\Bureau\\Projet Android\\app\\Databases\\database.sqlite";
    public static void addPlayer(String username, String country, int rating) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL)) {
            String sql = "INSERT INTO players (username, country, rating) VALUES (?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, username);
                statement.setString(2, country);
                statement.setInt(3, rating);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Player getPlayerByUsername(String username) {
        Player player = null;
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM players WHERE username = ?")) {
            statement.setString(1, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    String playerName = resultSet.getString("username");
                    String country = resultSet.getString("country");
                    int rating = resultSet.getInt("rating");
                    player = new Player(playerName, country, rating);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }

    public static void updatePlayerRating(String username, int newRating) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement = connection.prepareStatement("UPDATE players SET rating = ? WHERE username = ?")) {
            statement.setInt(1, newRating);
            statement.setString(2, username);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Game> gamesByPlayer(String username) {
        List<Game> games = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DATABASE_URL);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM games WHERE winner = ? OR loser = ? ORDER BY date")) {
            statement.setString(1, username);
            statement.setString(2, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    int gameId = resultSet.getInt("id");
                    String date = resultSet.getString("date");
                    String loser = resultSet.getString("loser");
                    String winner = resultSet.getString("winner");
                    String geoloc = resultSet.getString("geoloc");
                    String image = resultSet.getString("image");
                    games.add(new Game(gameId, date, loser, winner, geoloc, image));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return games;
    }

    public static void addGame(String winner, String loser, String date, String geoloc, String image) {
        try (Connection connection = DriverManager.getConnection(DATABASE_URL)) {
            String sql = "INSERT INTO games (winner, loser, date, geoloc, image) VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, winner);
                statement.setString(2, loser);
                statement.setString(3, date);
                statement.setString(4, geoloc);
                statement.setString(5, image);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(gamesByPlayer("loser"));
    }
}
