package com.example.projet_ac.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projet_ac.R;
import com.example.projet_ac.components.DeletePlayerDialog;
import com.example.projet_ac.entities.Player;
import com.example.projet_ac.services.LichessService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        LinearLayout playerLayout = (LinearLayout) findViewById(R.id.playerListLayout);

        LichessService chessService = new LichessService();

        List<Player> players = new ArrayList<>();
        players.add(new Player("Aimé","France",800));
        players.add(new Player("Magnus","Suède",3200));
        players.add(new Player("Alexis","France",1100));

        for(int i=0;i<10;i++)
        for(Player player : players){
            LinearLayout playerCardLayout = new LinearLayout(this);
            playerCardLayout.setOrientation(LinearLayout.HORIZONTAL);
            playerCardLayout.setBackgroundColor(Color.LTGRAY);
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            TextView text = new TextView(this);
            text.setTextSize(30);
            TextView text1 = new TextView(this);
            text1.setTextSize(20);
            text.setText(player.getUsername());
            text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            text1.setText("Elo : " + player.getRating());
            layout.addView(text);
            layout.addView(text1);

            LinearLayout layout2 = new LinearLayout(this);
            layout2.setOrientation(LinearLayout.HORIZONTAL);
            Button b1 = new Button(this);
            b1.setText("Match");
            b1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            Button b2 = new Button(this);
            b2.setText("X");
            //b2.setBackgroundColor(Color.RED);
            //b2.setTextColor(Color.WHITE);
            b2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DeletePlayerDialog dialog = new DeletePlayerDialog();
                    dialog.show(getSupportFragmentManager(), "DELETE");
                }
            });
            layout2.addView(b1);
            layout2.addView(b2);

            playerCardLayout.addView(layout);
            playerCardLayout.addView(layout2);
            playerLayout.addView(playerCardLayout);
        }
    }
}