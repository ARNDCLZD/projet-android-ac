package com.example.projet_ac.entities;

public class Player {
    String username;
    String country;
    int rating;


    public Player(String username, String country, int rating) {
        this.username = username;
        this.country = country;
        this.rating = rating;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
