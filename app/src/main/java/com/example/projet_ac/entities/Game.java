package com.example.projet_ac.entities;

public class Game {
    int id;
    String loser;
    String winner;
    String date;
    String geoloc;
    String image;

    public String getGeoloc() {
        return geoloc;
    }

    public void setGeoloc(String geoloc) {
        this.geoloc = geoloc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public Game(){}
    public Game(int id, String date, String loser, String winner, String geoloc, String image) {
        this.loser = loser;
        this.winner = winner;
        this.id = id;
        this.date = date;
        this.geoloc = geoloc;
        this.image = image;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
